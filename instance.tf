
###


resource "google_compute_network" "dataproc_network" {
  name                    = "dataproc-network"
  auto_create_subnetworks = true
}
resource "google_compute_firewall" "dataproc_allow_internal" {
  name    = "dataproc-allow-internal"
  network = google_compute_network.dataproc_network.self_link

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  allow {
    protocol = "udp"
    ports    = ["0-65535"]
  }

  source_ranges = ["10.128.0.0/9"]
   target_tags   = ["dataproc"]
}

resource "google_dataproc_cluster" "cluster" {
  name    = "test-dataproc-cluster"
  region  ="europe-west1"

  cluster_config {
    master_config {
      num_instances = 1
      machine_type  = "n1-standard-2"
    }

    worker_config {
      num_instances = 4
      machine_type  = "n1-standard-4"
    }

    software_config {
      image_version = "2.0.27-debian10"
    
    }

    gce_cluster_config {
      network = google_compute_network.dataproc_network.self_link
      service_account_scopes = [
        "https://www.googleapis.com/auth/cloud-platform",
      ]

      tags = ["dataproc"]
    }
  }
}


#cloud compser 

resource "google_dataproc_job" "pyspark_job" {
  region = "europe-west1"
  placement {
    cluster_name = google_dataproc_cluster.cluster.name
  }

  pyspark_config {
    main_python_file_uri = "gs://terraform-spark/create_file_tb.py"
    properties = {
      "spark.jars.packages" = "com.google.cloud.spark:spark-bigquery-with-dependencies_2.12:0.21.1"
    }


  }

}
