
#! usr/bin/python
# -*- coding: ISO-8859-1 -*-
# [START dataproc_pyspark_bigquery]
"""
Ce code Pyspark a plusieurs fonctions.
Il va inscrire dans le Data Lake (BigQuery) tous les trajets normalisés effectués.
Connecteurs Spark :
- BigQuery : gs://spark-lib/bigquery/spark-bigquery-latest_2.12.jar
! Cela va prendre plus de deux heures !
"""

import pyspark.sql.functions as func

from pyspark.sql import SparkSession, SQLContext
import os
import sys

from pyspark.sql import functions as func
from pyspark.sql import SparkSession
from pyspark.sql.window import Window

from datetime import datetime


# bucket name 

BUCKET_NAME = "terraform-spark"
BUCKET = "gs://{}".format(BUCKET_NAME)


# La table où seront stockés les trajets
TARGET_TABLE = "dbt.parquetsparktb"

spark = SparkSession \
    .builder \
    .appName("PySpark") \
    .getOrCreate()

sc = spark.sparkContext
sql_c = SQLContext(spark.sparkContext)
print("lecture file in dataproc")
data = sql_c.read.parquet(
    BUCKET + "/test_cout_bigquery_parquet/test_cout/data/*.parquet",
    header=True,
    sep=",")
print(" ...")

# Le bucket de transition
spark.conf.set('temporaryGcsBucket', BUCKET_NAME)

print("store data in bigquerry")
data.write.format('bigquery') \
    .option('table', TARGET_TABLE) \
    .save()



