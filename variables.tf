# Define input variable for a terraform module

variable "project_id" {

    default="vote-for-innov"
}

variable "region" {

    default = "europe-west1"
}