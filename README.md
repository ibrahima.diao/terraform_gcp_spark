# Projet Pipeline-spark-terraform

Ce projet utilise Terraform pour déployer des ressources sur Google Cloud Platform et exécute des scripts PySpark pour le traitement des données.

## Prérequis

- Un compte Google Cloud Platform (GCP) avec un projet configuré.
- Un compte GitLab avec un dépôt pour héberger votre code Terraform.
- Un fichier de clés de compte de service pour votre projet GCP (au format JSON).

## Structure du projet

### Description des fichiers

- `.gitlab-ci.yml` : Contient la configuration du pipeline GitLab CI/CD. Le pipeline est divisé en trois étapes : validate, plan et apply. Chaque étape exécute différentes commandes Terraform pour valider, planifier et appliquer les changements aux ressources GCP.
:

- `csv_to_parquet.py` : Convertit les fichiers CSV en fichiers Parquet.
- `create_events_data_file.py` : Transforme et traite les données pour les enregistrer dans Bigquery.

## Utilisation

1. Clonez ce dépôt dans votre propre compte GitLab.
2. Ajoutez vos clés de compte de service GCP au dépôt en tant que variables d'environnement. Vous pouvez le faire en accédant à l'onglet "Settings" > "CI/CD" > "Variables" de votre dépôt GitLab.
3. Modifiez les fichiers Terraform selon vos besoins. Assurez-vous de fournir des valeurs pour les variables définies dans `variables.tf`.
4. Poussez vos modifications dans votre dépôt GitLab.
5. Le pipeline GitLab se déclenchera automatiquement et créera les ressources sur GCP.
